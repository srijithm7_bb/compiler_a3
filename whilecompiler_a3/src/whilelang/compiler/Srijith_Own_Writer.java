package whilelang.compiler;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jasm.attributes.SourceFile;
import jasm.lang.Bytecode;
import jasm.lang.Bytecode.ArrayStore;
import jasm.lang.Bytecode.IfMode;
import jasm.lang.Bytecode.InvokeMode;
import jasm.lang.ClassFile;
import jasm.lang.ClassFile.Method;
import jasm.lang.JvmType;
import jasm.lang.JvmType.Array;
import jasm.lang.JvmType.Function;
import jasm.lang.JvmType.Int;
import jasm.lang.JvmTypes;
import jasm.lang.Modifier;

import whilelang.ast.*;

public class Srijith_Own_Writer {
	
	private static int CLASS_VERSION = 49;
	private static jasm.io.ClassFileWriter writer;
	HashMap<String,Type> declaredTypes;
	private HashMap<String,JvmType.Function> methodTypes;

	public static void main(String args[]) throws IOException
	{
		String filename = "tests/valid/aaaaaaaaaaaa.class";
		write(filename);
	}

	public Srijith_Own_Writer(String classFile) throws FileNotFoundException {
		writer = new jasm.io.ClassFileWriter(new FileOutputStream(classFile));
		declaredTypes = new HashMap<String,Type>();
		methodTypes = new HashMap<String,JvmType.Function>();
	}
	
	public static void write(String sourceFile) throws IOException {
		//System.out.println("filename: " + sourceFile.filename + "\n\n\n");
		File moduleName = new File(sourceFile);
		// Modifiers for class
		List<Modifier> modifiers = Arrays.asList(Modifier.ACC_PUBLIC, Modifier.ACC_FINAL);
		// List of interfaces implemented by class
		List<JvmType.Clazz> implemented = new ArrayList<JvmType.Clazz>();
		// Base class for this class
		JvmType.Clazz superClass = JvmTypes.JAVA_LANG_OBJECT;
		// The class name for this class
		JvmType.Clazz owner = new JvmType.Clazz(moduleName.getName());
		// Create the class!
		ClassFile cf = new ClassFile(CLASS_VERSION, owner, superClass, implemented, modifiers);
	
		Method m = new Method("", null, null);
		cf.attributes().add(new SourceFile("na"));
		
		writer.write(cf);
	}

}
